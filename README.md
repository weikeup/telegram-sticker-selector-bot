# Telegram 貼圖標籤管理器
## [@stiselbot](https://t.me/stiselbot)
為貼圖設定名稱及標籤，日後可以快速搜尋欲使用的貼圖。

## 使用方式
輸入 /new 並跟著步驟新增標籤，完成後直接向 Bot 傳送設定後的標籤文字，Bot 就會回傳相對應的貼圖。