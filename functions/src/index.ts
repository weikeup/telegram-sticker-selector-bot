import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import { Update } from 'telegram-typings'
import { Teletype, TgEvents } from 'teletype-telegram-bot-api'
import { botToken } from './env' // Import bot token

admin.initializeApp()
const firestore = admin.firestore()

enum Status {
    NEW,
    IDLE
}

interface UserStatus {
    userId: string
    status: Status,
    step: number,
    tempStickerId: string,
}

interface StickerTag {
    stickerId: string,
    fileId: string
    tags: Array<string>
}

let curStatus: UserStatus

const bot = new Teletype(botToken)

bot.onCommand("start", async (message) => {
    await bot.sendMessage(message.chat.id,
        `感謝您使用此機器人！
此機器人提供「將貼圖設定多個標籤，並使用標籤快速搜尋貼圖」之服務。

請輸入 /new 來新增貼圖標籤，或是輸入 /help 來取得更多說明。

Developer: @Weikeup
Source: https://gitlab.com/weikeup/telegram-sticker-selector-bot`
    )
})

bot.onCommand("help", async (message) => {
    await bot.sendMessage(message.chat.id,
        `輸入 /new 並跟著步驟新增標籤，完成後直接向 Bot 傳送設定後的標籤文字，Bot 就會回傳相對應的貼圖。

新增標籤途中傳送 /cancel 可以取消該動作。

如直接傳送貼圖，Bot 會回傳貼圖所對應到的標籤。`
    )
})

bot.onCommand("new", async (message) => {
    setStatus(Status.NEW)
    await bot.sendMessage(message.chat.id, "請傳送欲設定標籤的貼圖")
})

bot.onCommand("cancel", async (message) => {
    if (curStatus.tempStickerId !== "") {
        await firestore.doc(`User/${curStatus.userId}/Sticker/${curStatus.tempStickerId}`).delete()
    }
    setStatus(Status.IDLE)
    await bot.sendMessage(message.chat.id, "已取消操作")
})

bot.always(async (update) => {
    if (curStatus.status === Status.NEW && curStatus.step === 1) {
        if (update.message?.text !== undefined) {
            const tags = update.message.text.split(",").map((value) => { return value.trim() })
            await firestore.doc(`User/${curStatus.userId}/Sticker/${curStatus.tempStickerId}`).set({ tags: tags }, { merge: true })
            await bot.sendMessage(update.message.chat.id, "設定成功！")
            setStatus(Status.IDLE)
        }
    } else if (curStatus.status === Status.IDLE) {
        if (update.message?.text !== undefined) {
            const col = firestore.collection(`User/${curStatus.userId}/Sticker`)
            const text = update.message.text.trim()
            const result = await col.where("tags", "array-contains", text).get()
            for (const doc of result.docs) {
                await bot.sendSticker(update.message.chat.id, doc.get("fileId"))
            }
        }
    }
})

bot.on(TgEvents.STICKER, "search_sticker_tags", async (update) => {
    if (curStatus.status === Status.IDLE) {
        if (update.message?.sticker !== undefined) {
            const stickerId = (update.message.sticker as any).file_unique_id
            const col = firestore.collection(`User/${curStatus.userId}/Sticker`)
            const query = await col.where("stickerId", "==", stickerId).get()
            for (const doc of query.docs) {
                const tags = doc.get("tags") as Array<string>
                await bot.sendMessage(update.message.chat.id, tags.join(", "))
            }
        }
    }
})

bot.on(TgEvents.STICKER, "set_sticker", async (update) => {
    if (curStatus.status === Status.NEW && curStatus.step === 0) {
        if (update.message?.sticker !== undefined) {
            const col = firestore.collection(`User/${curStatus.userId}/Sticker`)
            const sticker = update.message.sticker
            const stickerId = (sticker as any).file_unique_id
            const stickerTag: StickerTag = {
                stickerId: stickerId,
                fileId: sticker.file_id,
                tags: []
            }
            await col.doc(stickerId).set(stickerTag)
            setStatus(Status.NEW, 1, stickerId)
            await bot.sendMessage(update.message.chat.id, "請傳送標籤名稱 (可用','分隔多個名稱)")
        }
    }
})

function setStatus(status: Status, step: number = 0, tempStickerId: string = "") {
    curStatus = {
        userId: curStatus.userId,
        status: status,
        step: step,
        tempStickerId: tempStickerId,
    }
}

async function readUserStatus(userId: string): Promise<UserStatus> {
    const doc = await firestore.doc(`User/${userId}`).get()
    const cStatus: UserStatus = doc.get("cur_status")
    if (cStatus !== undefined) {
        return cStatus
    } else {
        return {
            userId: userId,
            status: Status.IDLE,
            step: 0,
            tempStickerId: "",
        }
    }
}

async function saveUserStatus(userId: string, status: UserStatus) {
    await firestore.doc(`User/${userId}`).set({ cur_status: status }, { merge: true })
}

export const botHandle = functions.region("asia-east2").https.onRequest(async (req, resp) => {
    try {
        const update: Update = req.body
        const id = update.message!.chat.username?.toLowerCase() || update.message!.chat.id.toString()
        curStatus = await readUserStatus(id)
        await bot.processUpdate(update)
        await saveUserStatus(id, curStatus)
    } catch (err) {
        console.error(err)
    } finally {
        resp.end()
    }
})
